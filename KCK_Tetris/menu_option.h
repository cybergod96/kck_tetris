#pragma once
#include <string>
#include <iostream>

struct MenuOption
{
	std::string name;
	bool selected;

	void draw();
};
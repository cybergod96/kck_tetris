#pragma once
#include <iostream>
#include <conio.h>
#include <thread>
#include <mutex>
#include "tetris.h"
#include <Windows.h>

void logic();
void input();
void drawing();
void setCursorPosition(int x, int y);
bool redrawFlag;
#define SIGN_BOARD_FULL 'x'
#define SIGN_BOARD_EMPTY '.'
#define SIGN_PLAYERBLOCK_FULL '#'
#define SIGN_PLAYERBLOCK_EMPTY ' '

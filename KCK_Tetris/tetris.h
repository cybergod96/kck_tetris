#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <vector>
#include <fstream>
#include "menu_option.h"

#ifndef TETRIS_H

#define TETRIS_H

#define BOARD_WIDTH 10
#define BOARD_HEIGHT 24
#define INITIAL_FALLING_TIME 650
#define INITIAL_THRESHOLD 1000
#define THRESHOLD_INCREASE 1000

#define DIRECTION_DOWN 0
#define DIRECTION_LEFT 1
#define DIRECTION_RIGHT 2

extern bool patterns[19][4][4];

extern bool board[BOARD_WIDTH*BOARD_HEIGHT];

extern int player_block_x, player_block_y, player_block_type, player_block_pattern;
extern bool player_block_matrix[4][4];
extern int next_level_threshold, level, falling_time, next_block;
extern int points;

extern char txt_points[256], txt_level[32];

enum Game_State { GS_Menu, GS_Game, GS_GameOver, GS_Highscores };

extern Game_State game_state;

extern std::vector<int>highscores;

#define MENU_OPTIONS_COUNT 3
extern MenuOption options[MENU_OPTIONS_COUNT];
extern int current_option;

void set_tile(int x, int y, bool t);
bool get_tile(int x, int y);
void update_rotated_block();
void generate_player_block();
void place_player_block();
bool can_move_player_block(int dir);
int get_next_rotation();
int get_initial_block_pattern(int block_type);
bool can_rotate();
bool is_line_full(int index);
bool is_line_empty(int index);
bool check_game_over();
void fast_move_down();
void reset_player_block();
void add_points(int n);
void init();
void rotate();
void select_option(int n);
void load_highscores();
void save_highscores();

#endif // !TETRIS_H
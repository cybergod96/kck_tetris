#include "menu_option.h"

void MenuOption::draw()
{
	if (selected)
		std::cout << " > " << name << " <";
	else
		std::cout << " " << name;
}
#include "tetris.h"

bool patterns[19][4][4] =
{
	//d�ugi (2)
	{
		{ 0,1,0,0 },
{ 0,1,0,0 },
{ 0,1,0,0 },//0
{ 0,1,0,0 },
	},
	{
		{ 0,0,0,0 },
{ 0,0,0,0 },
{ 0,0,0,0 },//1
{ 1,1,1,1 },
	},
	//odwr�cone L (4)
	{

		{ 0,0,1,0 },
{ 0,0,1,0 },//2
{ 0,1,1,0 },
{ 0,0,0,0 },
	},
	{

		{ 1,0,0,0 },//3
{ 1,1,1,0 },
{ 0,0,0,0 },
{ 0,0,0,0 },
	},
	{

		{ 0,1,1,0 },
{ 0,1,0,0 },//4
{ 0,1,0,0 },
{ 0,0,0,0 },
	},
	{

		{ 0,1,1,1 },//5
{ 0,0,0,1 },
{ 0,0,0,0 },
{ 0,0,0,0 },
	},
	//L (4)
	{

		{ 0,1,0,0 },
{ 0,1,0,0 },//6
{ 0,1,1,0 },
{ 0,0,0,0 },
	},
	{

		{ 1,1,1,0 },//7
{ 1,0,0,0 },
{ 0,0,0,0 },
{ 0,0,0,0 },
	},
	{

		{ 0,1,1,0 },
{ 0,0,1,0 },//8
{ 0,0,1,0 },
{ 0,0,0,0 },
	},
	{

		{ 0,0,0,1 },//9
{ 0,1,1,1 },
{ 0,0,0,0 },
{ 0,0,0,0 },
	},
	//kwadrat (1)
	{

		{ 0,1,1,0 },//10
{ 0,1,1,0 },
{ 0,0,0,0 },
{ 0,0,0,0 },
	},
	//schodek (4)
	{

		{ 0,1,0,0 },//11
{ 1,1,1,0 },
{ 0,0,0,0 },
{ 0,0,0,0 },
	},
	{

		{ 0,1,0,0 },
{ 0,1,1,0 },//12
{ 0,1,0,0 },
{ 0,0,0,0 },
	},
	{

		{ 1,1,1,0 },//13
{ 0,1,0,0 },
{ 0,0,0,0 },
{ 0,0,0,0 },
	},
	{

		{ 0,0,1,0 },
{ 0,1,1,0 },//14
{ 0,0,1,0 },
{ 0,0,0,0 },
	},
	//z (2)
	{

		{ 1,1,0,0 },//15
{ 0,1,1,0 },
{ 0,0,0,0 },
{ 0,0,0,0 },
	},
	{

		{ 0,0,1,0 },
{ 0,1,1,0 },//16
{ 0,1,0,0 },
{ 0,0,0,0 },
	},
	//s (2)
	{

		{ 0,0,1,1 },//17
{ 0,1,1,0 },
{ 0,0,0,0 },
{ 0,0,0,0 },
	},
	{

		{ 0,1,0,0 },
{ 0,1,1,0 },//18
{ 0,0,1,0 },
{ 0,0,0,0 },
	},
};

int player_block_x = 0, player_block_y = 0, player_block_type = 0, player_block_pattern = 0;
bool player_block_matrix[4][4];
int next_level_threshold = 500, level = 1, falling_time = INITIAL_FALLING_TIME, next_block = 0;
Game_State game_state = GS_Menu;
int points = 0;
bool board[BOARD_WIDTH*BOARD_HEIGHT];
char txt_points[256], txt_level[32];
std::vector<int>highscores;

MenuOption options[MENU_OPTIONS_COUNT] = {
	{"New game",true},
	{"Highscores", false},
	{"Exit",false}
};
int current_option = 0;


void update_rotated_block()
{
	player_block_pattern = get_next_rotation();
	//update player_block_matrix
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			player_block_matrix[j][i] = patterns[player_block_pattern][i][j];
		}
	}
}

void generate_player_block()
{
	player_block_type = next_block;
	next_block = rand() % 7 + 1;
	player_block_pattern = get_initial_block_pattern(player_block_type);
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			player_block_matrix[j][i] = player_block_matrix[j][i] = patterns[player_block_pattern][i][j];
		}
	}
}

void fast_move_down()
{
	while (can_move_player_block(DIRECTION_DOWN))
	{
		player_block_y++;
	}
	place_player_block();
	reset_player_block();
	if (check_game_over())
		game_state = GS_GameOver;
}

bool can_move_player_block(int dir)
{
	for (int i = player_block_x; i <= player_block_x + 3; i++)
	{
		for (int j = player_block_y; j <= player_block_y + 3; j++)
		{
			if (player_block_matrix[i - player_block_x][j - player_block_y])
			{
				if (dir != DIRECTION_DOWN)
				{
					if (dir == DIRECTION_LEFT)
					{
						if (i - 1 < 0) return false;
						else if (get_tile(i - 1, j)) return false;
					}
					else if (dir == DIRECTION_RIGHT)
					{
						if (i + 1 >= BOARD_WIDTH) return false;
						else if (get_tile(i + 1, j)) return false;
					}
				}
				else if (get_tile(i, j + 1) || j + 1 >= BOARD_HEIGHT)
				{
					return false;
				}
			}
		}
	}
	return true;
}

void place_player_block()
{
	for (int i = player_block_x; i <= player_block_x + 3; i++)
	{
		for (int j = player_block_y; j <= player_block_y + 3; j++)
		{
			if (player_block_matrix[i - player_block_x][j - player_block_y])
				set_tile(i, j, true);
		}
	}
}

int get_initial_block_pattern(int block_type)
{
	switch (block_type)
	{
	case 1: return 0; break;
	case 2: return 2; break;
	case 3: return 6; break;
	case 4: return 10; break;
	case 5: return 11; break;
	case 6: return 15; break;
	case 7: return 17; break;
	}
	return -1;
}

void set_tile(int x, int y, bool t)
{
	board[y*BOARD_WIDTH + x] = t;
}

bool get_tile(int x, int y)
{
	return board[y*BOARD_WIDTH + x];
}

int get_next_rotation()
{
	switch (player_block_type)
	{
	case 1:
		return (player_block_pattern == 0) ? 1 : 0;
		break;
	case 2:
		if (player_block_pattern + 1 > 5) return 2;
		else return player_block_pattern + 1;
		break;
	case 3:
		if (player_block_pattern + 1 > 9) return 6;
		else return player_block_pattern + 1;
		break;
	case 4: return 10; break;
	case 5:
		if (player_block_pattern + 1 > 14) return 11;
		else return player_block_pattern + 1;
		break;
	case 6:
		return (player_block_pattern == 15) ? 16 : 15;
		break;
	case 7:
		return (player_block_pattern == 17) ? 18 : 17;
		break;
	}
}

bool is_line_full(int index)
{
	for (int i = 0; i < BOARD_WIDTH; i++)
	{
		if (!get_tile(i, index))
			return false;
	}
	return true;
}

bool is_line_empty(int index)
{
	for (int i = 0; i < BOARD_WIDTH; i++)
	{
		if (get_tile(i, index))
			return false;
	}
	return true;
}

bool can_rotate()
{
	int next_rotation = get_next_rotation();
	for (int i = player_block_x; i <= player_block_x + 3; i++)
	{
		for (int j = player_block_y; j <= player_block_y + 3; j++)
		{
			if (patterns[next_rotation][i - player_block_x][j - player_block_y])
			{
				if (player_block_x < 0 || player_block_x >= 7 || get_tile(i, j)) return false;
			}
		}
	}
	return true;
}

bool check_game_over()
{
	for (int i = player_block_x; i <= player_block_x + 3; i++)
	{
		for (int j = player_block_y; j <= player_block_y + 3; j++)
		{
			if (player_block_matrix[i - player_block_x][j - player_block_y] &&
				get_tile(i, j))
				return true;
		}
	}
	return false;
}

void reset_player_block()
{
	player_block_x = BOARD_WIDTH / 2 - 2;
	player_block_y = 0;
	generate_player_block();
}

void rotate()
{
	if (can_rotate())update_rotated_block();
	else
	{
		if (player_block_x < 0)
		{
			switch (player_block_type)
			{
			case 1:
				if (can_move_player_block(DIRECTION_RIGHT))
				{
					player_block_x++;
					update_rotated_block();
				}
				break;
			case 2:
				switch (player_block_pattern)
				{
				case 2:
					if (can_move_player_block(DIRECTION_RIGHT))
					{
						player_block_x++;
						update_rotated_block();
					}
					break;
				case 4: case 5:
					update_rotated_block();
					break;
				}
				break;
			case 3:
				switch (player_block_pattern)
				{
				case 6:
					if (can_move_player_block(DIRECTION_RIGHT))
					{
						player_block_x++;
						update_rotated_block();
					}
					break;
				case 8: case 9:
					update_rotated_block();
					break;
				}
				break;
			case 4:
				break;
			case 5:
				switch (player_block_pattern)
				{
				case 12: case 14:
					if (can_move_player_block(DIRECTION_RIGHT))
					{
						player_block_x++;
						update_rotated_block();
					}
					break;
				}
				break;
			case 6:
				switch (player_block_pattern)
				{
				case 16:
					if (can_move_player_block(DIRECTION_RIGHT))
					{
						player_block_x++;
						update_rotated_block();
					}
					break;
				}
				break;
			case 7:
				update_rotated_block();
				break;
			}

		}
		if (player_block_x >= 7)
		{
			int delta = 0, bx = player_block_x;
			bool cr = true;
			switch (player_block_type)
			{
			case 1:
				delta = player_block_x - 6;
				for (int i = 0; i < delta; i++)
				{
					if (can_move_player_block(DIRECTION_LEFT))
					{
						player_block_x--;
					}
					else
					{
						player_block_x = bx;
						cr = false;
					}
				}
				if (cr)
				{
					player_block_x = bx - delta;
					update_rotated_block();
				}
				break;
			case 2:
				switch (player_block_pattern)
				{
				case 2: case 3:
					update_rotated_block();
					break;
				case 4:
					if (can_move_player_block(DIRECTION_LEFT))
						player_block_x--;
					update_rotated_block();
					break;
				}
				break;
			case 3:
				switch (player_block_pattern)
				{
				case 6: case 7:
					update_rotated_block();
					break;
				case 8:
					if (can_move_player_block(DIRECTION_LEFT))
						player_block_x--;
					update_rotated_block();
					break;
				}
				break;
			case 4:
				break;
			case 5:
				switch (player_block_pattern)
				{
				case 11: case 12: case 13: case 14:
					update_rotated_block();
					break;
				}
				break;
			case 6:
				switch (player_block_pattern)
				{
				case 15: case 16:
					update_rotated_block();
					break;
				}
				break;
			case 7:
				switch (player_block_pattern)
				{
				case 18:
					if (can_move_player_block(DIRECTION_LEFT))
					{
						player_block_x--;
						update_rotated_block();
					}
					break;
				}
				break;
			}
		}
	}
}

void init()
{
	next_level_threshold = 500;
	level = 1;
	falling_time = INITIAL_FALLING_TIME;
	points = 0;
	snprintf(txt_points, sizeof(txt_points), "Points: %d", points);
	snprintf(txt_level, sizeof(txt_level), "Level: %d", level);

	next_block = rand() % 7 + 1;
	reset_player_block();

	game_state = GS_Menu;
	current_option = 0;

	for (int i = 0; i < BOARD_HEIGHT; i++)
	{
		for (int j = 0; j < BOARD_WIDTH; j++)
		{
			set_tile(j, i, false);
		}
	}
}

void add_points(int n)
{
	points += n;
	//update points text
	snprintf(txt_points, sizeof(txt_points), "Points: %d", points);
}

void select_option(int n)
{
	for (int i = 0; i < MENU_OPTIONS_COUNT; i++)
	{
		options[i].selected = i == n;
	}
}

void load_highscores()
{
	std::ifstream in("highscores.txt");
	if (!in.good()) return;
	highscores.clear();
	int x;
	while (!in.eof())
	{
		in >> x;
		highscores.push_back(x);
	}
	in.close();
}

void save_highscores()
{
	std::ofstream out("highscores.txt");
	if (!out.good()) return;
	for (int i = 0; i < highscores.size(); i++)
	{
		out << highscores[i] << std::endl;
	}
	out.close();
}
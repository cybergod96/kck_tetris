#include "main.h"
#include <algorithm>
void setCursorPosition(int x, int y)
{
	static const HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	std::cout.flush();
	COORD coord = { (SHORT)x, (SHORT)y };
	SetConsoleCursorPosition(hOut, coord);
}

void drawing()
{
	for(;;)
	{
		if(game_state == GS_Game)
		{
			if (redrawFlag)
			{
				//dispplay board and player block
				system("cls");
				for (int i = 0; i < BOARD_HEIGHT; i++)
				{
					for (int j = 0; j < BOARD_WIDTH; j++)
					{
						if ((j >= player_block_x && j <= player_block_x + 3 &&
							i >= player_block_y && i <= player_block_y + 3))
						{
							if (player_block_matrix[j - player_block_x][i - player_block_y]) {
								SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), BACKGROUND_GREEN | FOREGROUND_GREEN);
								std::cout << SIGN_PLAYERBLOCK_FULL;
								SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
							}
							else
							{
								if (get_tile(j, i)) std::cout << SIGN_BOARD_FULL;
								else std::cout << SIGN_BOARD_EMPTY;
							}
						}
						else
						{
							if (get_tile(j, i)) {
								SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), BACKGROUND_GREEN | FOREGROUND_GREEN);
								std::cout << SIGN_BOARD_FULL;
								SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
							}
							else std::cout << SIGN_BOARD_EMPTY;

						}
					}
					std::cout << "\n";
				}

				setCursorPosition(BOARD_WIDTH + 4, 2);
				std::cout << txt_level;
				setCursorPosition(BOARD_WIDTH + 4, 3);
				std::cout << txt_points;
				setCursorPosition(BOARD_WIDTH + 4, 4);
				std::cout << "Next: ";
				for (int i = 0; i < 4; i++)
				{
					setCursorPosition(BOARD_WIDTH + 4, 5 + i);
					for (int j = 0; j < 4; j++)
					{
						if (patterns[get_initial_block_pattern(next_block)][i][j])
						{		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), BACKGROUND_GREEN | FOREGROUND_GREEN);
						std::cout << SIGN_PLAYERBLOCK_FULL;
						SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
						}
						else std::cout << SIGN_PLAYERBLOCK_EMPTY;
					}
				}
				redrawFlag = false;
				
			}
		}
		else if (game_state == GS_Menu)
		{
			if (redrawFlag)
			{
				system("cls");
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), (rand()%15)+1);
					std::cout << "   _________  ________  _________  _______     _____   ______  \n";
					std::cout << "  |  _   _  ||_   __  ||  _   _  ||_   __ \\   |_   _|.' ____ \\ \n";
					std::cout << "  |_/ | | \\_|  | |_ \\_||_/ | | \\_|  | |__) |    | |  | (___ \\_|\n";
					std::cout << "      | |      |  _| _     | |      |  __ /     | |   _.____`. \n";
					std::cout << "      | |_     | |__/ |   _| |_    _| |  \\ \\_  _| |_ | \\____) |\n";
					std::cout << "    |_____|  |________|  |_____|  |____| |___||_____| \\______.'\n\n";
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);

				for (int i = 0; i < MENU_OPTIONS_COUNT; i++)
				{ 
					options[i].draw();
					std::cout << "\n";
				}
				redrawFlag = false;
			}
		}
		else if (game_state == GS_GameOver)
		{
			if (redrawFlag)
			{
				system("cls");
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), (rand() % 15) + 1);
				std::cout << "	  _____            __  __ ______ ______      ________ _____\n";
				std::cout << "	 / ____|    /\\    |  \\/  |  ____/ __ \\ \\    / /  ____| __  \\\n";
				std::cout << " 	 | |  __   /  \\   | \\  / | |__ | |  | \\ \\  / /| |__  | |__) |\n";
				std::cout << "	 | | |_ | / /\\ \\  | |\\/| |  __|| |  | |\\ \\/ / |  __| |  _  /\n";
				std::cout << "	 | |__| |/ ____ \\ | |  | | |___| |__| | \\  /  | |____| | \\ \\\n";
				std::cout << "	 \\_____ /_ /   \\_\\|_|  |_|______\\_____/  \\/   |______|_|  \\_\\\n";
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
				std::cout << "Your score:" << points << "\nPress ENTER to return to main menu...";
				redrawFlag = false;
			}
		}
		else if (game_state == GS_Highscores)
		{
			if (redrawFlag)
			{
				system("cls");
				if (highscores.empty())
					std::cout << "There are no highscores yet.\n";
				else
				{

					std::cout << "+------------ +\n| Highscores  |\n+------------ +\n";
					for (int i = 0; i < highscores.size(); i++)
					{
						std::cout << "+ " << i << "  + " << highscores[i] << "   +\n";
					}
					std::cout << "+---- + ----- +" << std::endl;
				}
				std::cout << "Press ENTER to return to main menu...";
				redrawFlag = false;
			}
		}

		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
}

void logic()
{
	for (;;)
	{
		if(game_state == GS_Game)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(falling_time));
			if (can_move_player_block(DIRECTION_DOWN)) player_block_y++;
			else
			{
				place_player_block();
				reset_player_block();
				if (check_game_over())
				{
					game_state = GS_GameOver;
					redrawFlag = true;
				}
			}

			for (int i = 0; i < BOARD_HEIGHT; i++)
			{
				if (is_line_full(i))
				{
					//remove line
					for (int x = 0; x < BOARD_WIDTH; x++)
					{
						set_tile(x, i, false);
					}

					//move the rest of the board down
					for (int y = i - 1; y >= 0; y--)
					{
						for (int x = 0; x < BOARD_WIDTH; x++)
						{
							if (get_tile(x, y) && (!get_tile(x, y + 1) && y + 1 < BOARD_HEIGHT))
							{
								set_tile(x, y + 1, true);
								set_tile(x, y, false);
							}
						}
					}
					add_points(25);
					if (points >= next_level_threshold)
					{
						level++;
						falling_time -= 50;
						next_level_threshold += THRESHOLD_INCREASE;
						snprintf(txt_level, sizeof(txt_level), "Level: %d", level);
					}
				}
			}
			redrawFlag = true;
		}
		else
		{
			std::this_thread::sleep_for(std::chrono::microseconds(50));
		}
	}
}

void input()
{
	for (;;)
	{	
		if(game_state == GS_Game)
		{
			switch (_getch())
			{
			case 'w':
				rotate();
				redrawFlag = true;
				break;
			case 's':
				fast_move_down();
				redrawFlag = true;
				break;
			case 'a':
				if (can_move_player_block(DIRECTION_LEFT))
					player_block_x--;
				redrawFlag = true;
				break;
			case 'd':
				if (can_move_player_block(DIRECTION_RIGHT))
					player_block_x++;
				redrawFlag = true;
				break;
			default: break;
			}
		}
		else if (game_state == GS_Menu)
		{
			switch (_getch())
			{
			case 'w':
				current_option = (current_option - 1) % MENU_OPTIONS_COUNT;
				select_option(current_option);
				redrawFlag = true;
				break;
			case 's':
				current_option = (current_option + 1) % MENU_OPTIONS_COUNT;
				select_option(current_option);
				redrawFlag = true;
				break;
			case '\r':
				switch (current_option)
				{
				case 0: game_state = GS_Game; break;
				case 1: game_state = GS_Highscores; break;
				case 2:
					save_highscores();
					exit(0); 
					break;
				default: break;
				}
				redrawFlag = true;
				break;
			}
		}
		else if (game_state == GS_GameOver)
		{
			if (_getch() == '\r')
			{
				highscores.push_back(points);
				std::sort(highscores.rbegin(), highscores.rend());
				if (highscores.size() > 10)
					highscores.pop_back();
				game_state = GS_Menu;
				init();
				redrawFlag = true;
			}
		}
		else if (game_state == GS_Highscores)
		{
			if (_getch() == '\r')
			{
				game_state = GS_Menu;
				redrawFlag = true;
			}
		}
			
		
		
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
}

int main()
{
	srand(time(NULL));
	init();
	load_highscores();
	redrawFlag = true;
	std::thread thread_logic(logic);
	std::thread thread_input(input);
	std::thread thread_drawing(drawing);
	thread_logic.join();
	thread_input.join();
	thread_drawing.join();
    return 0;
}

